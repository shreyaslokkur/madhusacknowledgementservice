
package com.lks.notifications;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lks.MASManagementProperties;
import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.error.MASErrorSeverity;
import com.lks.models.SMSBalance;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SMSBalanceNotification {

    @Autowired
    private MASManagementProperties masManagementProperties;

    private static final Logger logger = LoggerFactory.getLogger(SMSBalanceNotification.class);


    public SMSBalance checkBalance() {
        try {
            logger.info("Entered checkSMS Balance");
            // Construct data
            String apiKey = "apikey=" + masManagementProperties.getSmsAPIKey();

            // Send data
            String data = apiKey;
            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/balance/?").openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();

            SMSBalance smsBalance = new ObjectMapper().readValue(stringBuffer.toString(), SMSBalance.class);
            logger.info("The SMS balance is: {}", smsBalance);
            return smsBalance;
        } catch (Exception e) {
            logger.error("Unable to retrieve SMS balance {}", e);
            throw new MASException(MASErrorCodes.SMS_NOTIFICATION_ERROR, MASErrorSeverity.LEVEL_3, "UNABLE TO RETRIEVE SMS BALANCE");
        }
    }


}
