
package com.lks.notifications;

import com.lks.MASManagementProperties;
import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.error.MASErrorSeverity;
import com.lks.models.TransactionDTO;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SMSNotification {

    @Autowired
    private MASManagementProperties masManagementProperties;

    private static final Logger logger = LoggerFactory.getLogger(SMSNotification.class);

    public void sendSms(TransactionDTO transactionDTO) {
        logger.info("Entering SMS Notification: {}", transactionDTO);
        logger.info("Attempting to send SMS to the number: "+ transactionDTO.getPhoneNumber());
        try {
            // Construct data

            String apiKey = "apiKey=" + masManagementProperties.getSmsAPIKey();
            String message = "&message=" + generateSMSMessage(transactionDTO.isRepeat());
            String sender = "&sender=" + masManagementProperties.getSmsSender();
            String numbers = "&numbers=" + transactionDTO.getPhoneNumber();

            // Send data
            HttpURLConnection conn = (HttpURLConnection) new URL("http://api.textlocal.in/send/?").openConnection();
            String data = apiKey + numbers + message + sender;
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();
            logger.info("Exiting SMS Notification");

        } catch (Exception e) {
            logger.error("Unable to send sms to the number: {}. Exception {}", transactionDTO.getPhoneNumber(), e);
            throw new MASException(MASErrorCodes.SMS_NOTIFICATION_ERROR, MASErrorSeverity.LEVEL_2, "Unable to send sms to the number: "+ transactionDTO.getPhoneNumber());

        }
    }


    private String generateSMSMessage(boolean repeat) {
        Map<String, String> valuesMap = new HashMap<String, String>();
        String resolvedMessageString = null;
        valuesMap.put("googleUrl", masManagementProperties.getGoogleUrl());
        valuesMap.put("facebookUrl", masManagementProperties.getFacebookUrl());
        StrSubstitutor sub = new StrSubstitutor(valuesMap);
        if(repeat) {
            resolvedMessageString = sub.replace(masManagementProperties.getSmsRepeatMessage());
        } else {
            resolvedMessageString = sub.replace(masManagementProperties.getSmsMessage());
        }
        logger.info("The SMS text which will be sent is: "+ resolvedMessageString);
        return resolvedMessageString;
    }
}
