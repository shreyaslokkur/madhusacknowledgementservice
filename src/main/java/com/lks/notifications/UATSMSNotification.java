
package com.lks.notifications;

import com.lks.MASManagementProperties;
import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.error.MASErrorSeverity;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class UATSMSNotification {

    @Autowired
    private MASManagementProperties masManagementProperties;

    private static final Logger logger = LoggerFactory.getLogger(UATSMSNotification.class);

    public void sendSms(int numberOfCustomers) {
        logger.info("Entering UAT SMS Notification: ");
        try {
            // Construct data
            String userName = "username=" + masManagementProperties.getSmsUsername();
            String hash = "&hash=" + masManagementProperties.getSmsHash();
            String message = "&message=" + generateSMSMessage(numberOfCustomers);
            String sender = "&sender=" + masManagementProperties.getSmsSender();
            String numbers = "&numbers=" + masManagementProperties.getUatSmsNumber();

            // Send data
            HttpURLConnection conn = (HttpURLConnection) new URL("http://api.textlocal.in/send/?").openConnection();
            String data = userName + hash + numbers + message + sender;
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();
            logger.info("Exiting SMS Notification");

        } catch (Exception e) {
            logger.error("Unable to send UAT sms to the admin {}", e);
            throw new MASException(MASErrorCodes.SMS_NOTIFICATION_ERROR, MASErrorSeverity.LEVEL_2, "Unable to send UAT sms to the admin");

        }
    }


    private String generateSMSMessage(int numberOfCustomers) {
        Map<String, String> valuesMap = new HashMap<String, String>();
        String resolvedMessageString = null;
        valuesMap.put("numberOfCustomers", Integer.toString(numberOfCustomers));
        StrSubstitutor sub = new StrSubstitutor(valuesMap);
        resolvedMessageString = sub.replace(masManagementProperties.getUatSmsMessage());
        logger.info("The SMS text which will be sent is: "+ resolvedMessageString);
        return resolvedMessageString;
    }
}
