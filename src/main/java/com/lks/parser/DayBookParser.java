package com.lks.parser;

import com.lks.core.MASConstants;
import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.core.MyThreadLocal;
import com.lks.error.MASErrorSeverity;
import com.lks.models.DayBookDTO;
import com.lks.models.TransactionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 9/4/17
 * Time: 4:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class DayBookParser {

    static Logger logger = LoggerFactory.getLogger(DayBookParser.class);

    private static Map<Integer, DayBookDTO> basicBuyerAddressMap = new HashMap<>();

    private static final String nameRegEx = "^[\\pL\\s]+$";
    private static final String vehicleNumberRegex = "(([A-Za-z]){2,3}( |-)(?:[0-9]){1,2}( |-)(?:[A-Za-z]){0,2}( |-)([0-9]){1,4})";
    private static final String phoneNumberRegex = "(?:[\\d]){10,11}$";
    private static final String emailAddressRegex = "(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    private static final String odometerRegex = "\\D*(\\d+)\\D*";
    private static final String vehicleNameRegex = "^(VEH|veh|Veh|VEh)( ?)(:|;|-)";

    private static final int nameRegExGroupIndex = 0;
    private static final int vehicleNumberRegexGroupIndex = 0;
    private static final int phoneNumberRegexGroupIndex = 0;
    private static final int emailAddressRegexGroupIndex = 0;
    private static final int odometerRegexGroupIndex = 1;

    private static final int vehicleBrandNamePosition = 0;
    private static final int vehicleModelNamePosition = 1;


    public List<TransactionDTO> parseDayBook(File inputFile) {
        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DayBookHandler dayBookHandler = new DayBookHandler();
            saxParser.parse(inputFile, dayBookHandler);

            logger.info("Map: {}", basicBuyerAddressMap);
            List<TransactionDTO> notificationObject = createNotificationObject();
            return notificationObject;

        } catch (Exception e) {
            logger.error("Error parsing the daybook xml file: {}", inputFile.getAbsolutePath());
            throw new MASException(MASErrorCodes.PARSING_ERROR, MASErrorSeverity.LEVEL_3, "Error parsing the daybook xml file: "+ inputFile.getAbsolutePath(), e);
        }

    }

    private List<TransactionDTO> createNotificationObject() {
        List<TransactionDTO> transactionDTOList = new ArrayList<>();
        TransactionDTO transactionDTO = null;
        for(Integer i : basicBuyerAddressMap.keySet()) {
            DayBookDTO dayBookObject = basicBuyerAddressMap.get(i);
            try{
                transactionDTO = new TransactionDTO();

                List<String> basicBuyerAddressList = dayBookObject.getAddressList();
                boolean vehicleNumberIsSet = false;
                boolean phoneNumberIsSet = false;
                boolean emailAddressIsSet = false;
                boolean odometerReadingIsSet = false;
                boolean vehicleNameIsSet = false;
                transactionDTO.setName(dayBookObject.getName());


                for(String basicBuyerAddress : basicBuyerAddressList) {
                    if(basicBuyerAddress.equals("NA")) {
                        continue;
                    }

                    if(!vehicleNumberIsSet) {
                        String vehicleNumber = isPattern(vehicleNumberRegex, basicBuyerAddress, vehicleNumberRegexGroupIndex);
                        if(vehicleNumber != null) {
                            vehicleNumber = vehicleNumber.replaceAll("\\s","");
                            vehicleNumber = vehicleNumber.replaceAll("-","");
                            transactionDTO.setVehicleNumber(vehicleNumber);
                            vehicleNumberIsSet = true;
                        }
                    }
                    if(!phoneNumberIsSet) {
                        String isPhoneNumber = isPattern(phoneNumberRegex, basicBuyerAddress, phoneNumberRegexGroupIndex);
                        if(isPhoneNumber != null) {
                            transactionDTO.setPhoneNumber(isPhoneNumber);
                            phoneNumberIsSet = true;
                        }
                    }

                    if(!emailAddressIsSet) {
                        String emailAddressProbable = basicBuyerAddress.toLowerCase();
                        String isEmailAddress = isPattern(emailAddressRegex, emailAddressProbable, emailAddressRegexGroupIndex);
                        if(isEmailAddress != null) {
                            transactionDTO.setEmailAddress(isEmailAddress);
                            emailAddressIsSet = true;
                        }
                    }

                    if(!odometerReadingIsSet) {
                        String odometerReadingInLowerCase = basicBuyerAddress.toLowerCase();
                        int indexOfKm = odometerReadingInLowerCase.indexOf("km");
                        if (indexOfKm != -1) {
                            String odometerStringReading = isPattern(odometerRegex, basicBuyerAddress, odometerRegexGroupIndex);
                            if (odometerStringReading != null) {
                                transactionDTO.setOdometerReading(Double.parseDouble(odometerStringReading));
                                odometerReadingIsSet = true;
                            }

                        }
                    }

                    if(!vehicleNameIsSet) {

                        String vehicleNameProbable = basicBuyerAddress;
                        boolean isVehicleName = matchesPattern(vehicleNameRegex, vehicleNameProbable);
                        if(isVehicleName) {
                            String vehicleFullName = vehicleNameProbable.replaceFirst(vehicleNameRegex, "");
                            String[] vehicleNameSplit = vehicleFullName.trim().split("\\s+");
                            if(vehicleNameSplit != null) {
                                try{
                                    transactionDTO.setVehicleBrandName(vehicleNameSplit[vehicleBrandNamePosition]);
                                    transactionDTO.setVehicleModelName(vehicleNameSplit[vehicleModelNamePosition]);
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    logger.error("Could not store vehicle brand or model name: {}", vehicleNameProbable);
                                }



                            }


                        }

                    }

                }

                if(!vehicleNumberIsSet) {
                    logger.error("Vehicle Number was not inputed. Not sending a notification to this user");
                    throw new MASException(MASErrorCodes.BAD_INPUT, MASErrorSeverity.LEVEL_3, "Vehicle number is not present "+ transactionDTO);
                } else {
                    transactionDTOList.add(transactionDTO);
                }

            } catch (Exception e) {
                logger.error("Encountered exception storing the Voucher details: {}", dayBookObject);
                List<MASException> masExceptions = MyThreadLocal.get().getExceptionList();
                if(masExceptions == null) {
                    masExceptions = new ArrayList<>();
                }
                masExceptions.add(new MASException(MASErrorCodes.PARSING_ERROR, MASErrorSeverity.LEVEL_3, "Encountered exceptions storing voucher details: "+ dayBookObject,e));
            }

        }
        return transactionDTOList;
    }

    private String isPattern(String pattern, String line, int groupIndex) {
        if(line != null) {
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(line);
            if(m.find()) {
                return m.group(groupIndex);
            } else {
                return null;
            }
        }
        return null;

    }

    private boolean matchesPattern(String pattern, String line) {
        if(line != null) {
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(line);
            if(m.find()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }



    static class DayBookHandler extends DefaultHandler {

        private boolean basicBuyerAddress = false;
        private boolean basicBuyerName = false;
        private int count;
        private List<String> addressList;
        private String name;
        private DayBookDTO dayBookObject;



        @Override
        public void startElement(String uri, String localName,String qName,
                                 Attributes attributes) throws SAXException {

            if (qName.equalsIgnoreCase(MASConstants.VOUCHER)) {
                dayBookObject = new DayBookDTO();
            }

            if (qName.equalsIgnoreCase(MASConstants.BASICBUYERNAME)) {
                basicBuyerName = true;
            }

            if (qName.equalsIgnoreCase(MASConstants.BASICBUYERADDRESSLIST)) {
                addressList = new ArrayList<>();
            }

            if (qName.equalsIgnoreCase(MASConstants.BASICBUYERADDRESS)) {
                basicBuyerAddress = true;
            }
        }

        @Override
        public void endElement(String uri, String localName,
                               String qName) throws SAXException {

            if(qName.equalsIgnoreCase(MASConstants.BASICBUYERADDRESSLIST)) {
                dayBookObject.setAddressList(addressList);
            }

            if (qName.equalsIgnoreCase(MASConstants.BASICBUYERNAME)) {
                dayBookObject.setName(name);

            }

            if (qName.equalsIgnoreCase(MASConstants.VOUCHER)) {
                if(dayBookObject.getAddressList() != null && dayBookObject.getAddressList().size() > 0) {
                    basicBuyerAddressMap.put(count, dayBookObject);
                    count++;
                } else {
                    dayBookObject = null;
                }
            }
        }

        @Override
        public void characters(char ch[], int start, int length) throws SAXException {

            String addressDetails = null;
            if(basicBuyerName) {
                name = new String(ch, start, length);
                basicBuyerName = false;
            }
            if (basicBuyerAddress) {
                addressDetails =  new String(ch, start, length);
                addressList.add(addressDetails);
                basicBuyerAddress = false;
            }

        }

    }


}
