package com.lks;

import com.lks.admin.AdminEmailNotification;
import com.lks.admin.AdminService;
import com.lks.admin.ExcelGenerator;
import com.lks.connector.TallyConnector;
import com.lks.core.FilterationService;
import com.lks.db.dao.UserDAO;
import com.lks.db.dao.impl.UserDAOImpl;
import com.lks.error.ErrorHandler;
import com.lks.db.dao.NotificationDAO;
import com.lks.db.dao.impl.NotificationDAOImpl;
import com.lks.error.WarningHandler;
import com.lks.generator.AcknowledgementServiceGenerator;
import com.lks.notifications.*;
import com.lks.parser.DayBookParser;
import com.lks.scheduler.MASWorker;
import com.lks.service.NotificationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BeanConfiguration {




    @Bean
    public EmailNotification getEmailNotification() {
        return new EmailNotification();
    }


    @Bean
    public AcknowledgementServiceGenerator getPortfolioGenerator() {
        return new AcknowledgementServiceGenerator();
    }

    @Bean
    public MASWorker getTallyWorker() {
        return new MASWorker();
    }

    @Bean
    public SMSNotification getSMSNotification() { return new SMSNotification(); }

    @Bean
    public MASManagementProperties getMASManagementProperties() { return new MASManagementProperties(); }

    @Bean
    public ErrorHandler getErrorHandler() { return new ErrorHandler(); }

    @Bean
    public NotificationDAO getNotificationDAO() {
        return new NotificationDAOImpl();
    }

    @Bean
    public UserDAO getUserDAO() {
        return new UserDAOImpl();
    }

    @Bean
    public NotificationService getNotificatonService() {
        return new NotificationService();
    }

    @Bean
    public AdminService getAdminService() {
        return new AdminService();
    }

    @Bean
    public AdminEmailNotification getAdminEmailNotification() {
        return new AdminEmailNotification();
    }

    @Bean
    public ExcelGenerator getExcelGenerator() {
        return new ExcelGenerator();
    }

    @Bean
    public DayBookParser getDayBookPaser() {
        return new DayBookParser();
    }

    @Bean
    public TallyConnector getTallyConnector() {
        return new TallyConnector();
    }

    @Bean
    public UATEmailNotification getUatEmailNotification(){
        return new UATEmailNotification();
    }

    @Bean
    public UATSMSNotification getUatSMSNotification() {
        return new UATSMSNotification();
    }

    @Bean
    public FilterationService getFilterationService() {
        return new FilterationService();
    }

    @Bean
    public WarningHandler getWarningHandler() { return new WarningHandler();}

    @Bean
    public SMSBalanceNotification getSMSBalanceNotification(){ return new SMSBalanceNotification();}


}
