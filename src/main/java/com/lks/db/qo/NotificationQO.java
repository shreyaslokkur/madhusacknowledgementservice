
package com.lks.db.qo;

public class NotificationQO {

    private int id;
    private int userId;
    private double odometerReading;
    private String serviceType;
    private String notificationStatus;
    private long createdDts;
    private long notificationDts;
    private boolean isRepeat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(double odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public long getCreatedDts() {
        return createdDts;
    }

    public void setCreatedDts(long createdDts) {
        this.createdDts = createdDts;
    }

    public long getNotificationDts() {
        return notificationDts;
    }

    public void setNotificationDts(long notificationDts) {
        this.notificationDts = notificationDts;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public void setRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }

    @Override
    public String toString() {
        return "NotificationQO{" +
                "id=" + id +
                ", userId=" + userId +
                ", odometerReading=" + odometerReading +
                ", serviceType='" + serviceType + '\'' +
                ", notificationStatus='" + notificationStatus + '\'' +
                ", createdDts=" + createdDts +
                ", notificationDts=" + notificationDts +
                ", isRepeat=" + isRepeat +
                '}';
    }
}
