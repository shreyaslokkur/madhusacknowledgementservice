package com.lks.db.dao.rowmapper;

import com.lks.db.qo.UserQO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by lokkur
 */
public class UserRowMapper implements RowMapper<UserQO> {

    private static final String ID = "ID";
    private static final String NAME = "NAME";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    private static final String VEHICLE_NUMBER = "VEHICLE_NUMBER";
    private static final String VEHICLE_BRAND = "VEHICLE_BRAND";
    private static final String VEHICLE_MODEL = "VEHICLE_MODEL";
    private static final String CREATED_DTS = "CREATED_DTS";

    @Override
    public UserQO mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserQO userQO = null;
        if (rs != null) {
        userQO = new UserQO();
        userQO.setId(rs.getInt(ID));
        userQO.setName(rs.getString(NAME));
        userQO.setPhoneNumber(rs.getString(PHONE_NUMBER));
        userQO.setEmailAddress(rs.getString(EMAIL_ADDRESS));
        userQO.setVehicleNumber(rs.getString(VEHICLE_NUMBER));
        userQO.setVehicleBrand(rs.getString(VEHICLE_BRAND));
        userQO.setVehicleModel(rs.getString(VEHICLE_MODEL));
        userQO.setCreatedDts(rs.getLong(CREATED_DTS));
        }
        return userQO;
    }
}
