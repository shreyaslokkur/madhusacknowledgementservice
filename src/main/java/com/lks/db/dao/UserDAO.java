package com.lks.db.dao;


import com.lks.db.qo.UserQO;

import java.util.List;

/**
 * Created by lokkur
 */
public interface UserDAO {

    int addUser(UserQO userQO);

    UserQO getUserByIdForRead(int userId);
    UserQO getUserByVehicleNumberForRead(String vehicleNumber);

    List<UserQO> getValidVehicleNumberPresentInList(List<String> licenseNumberList);

    boolean updateUser(UserQO userQO);

}
