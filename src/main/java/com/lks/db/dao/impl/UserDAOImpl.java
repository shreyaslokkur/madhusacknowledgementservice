package com.lks.db.dao.impl;

import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.db.dao.UserDAO;
import com.lks.db.dao.rowmapper.UserRowMapper;
import com.lks.db.qo.UserQO;
import com.lks.error.MASErrorSeverity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lokkur on 31/03/17.
 */
public class UserDAOImpl implements UserDAO {
    private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final String SQL_CREATE_USER = "INSERT INTO USER "
            + "(NAME, PHONE_NUMBER, EMAIL_ADDRESS, VEHICLE_NUMBER, VEHICLE_BRAND, VEHICLE_MODEL, CREATED_DTS) VALUES "
            + "(:name, :phoneNumber, :emailAddress, :vehicleNumber, :vehicleBrand, :vehicleModel, :createdDts);";

    private static final String SQL_GET_USER = "SELECT * FROM USER ";

    private static final String SQL_GET_USER_FOR_ID_FOR_READ = "SELECT U.* " +
            "FROM USER U " +
            "WHERE U.ID = :id";

    private static final String SQL_GET_USER_FOR_VEHICLE_NUMBER_FOR_READ = "SELECT U.* " +
            "FROM USER U " +
            "WHERE U.VEHICLE_NUMBER = :vehicleNumber";


    private static final String SQL_GET_LIST_OF_VALID_VEHICLE_NUMBER_IN_DATABASE = "SELECT U.* " +
            "FROM USER U " +
            "WHERE U.VEHICLE_NUMBER in (:vehicleNumberList)";

    private static final String SQL_UPDATE_USER = "UPDATE USER " +
            "SET EMAIL_ADDRESS = IFNULL(:emailAddress , EMAIL_ADDRESS), PHONE_NUMBER = IFNULL(:phoneNumber , PHONE_NUMBER) " +
            "WHERE ID = :id;";



    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String EMAIL_ADDRESS = "emailAddress";
    private static final String VEHICLE_NUMBER = "vehicleNumber";
    private static final String VEHICLE_BRAND = "vehicleBrand";
    private static final String VEHICLE_MODEL = "vehicleModel";
    private static final String CREATED_DTS = "createdDts";

    @Override
    @Transactional
    public int addUser(UserQO userQO) {
        logger.info("Entering addUser {}", userQO);
        try {
            MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            long currentTimeMillis = System.currentTimeMillis();

            namedParameters.addValue(NAME, userQO.getName());
            namedParameters.addValue(PHONE_NUMBER, userQO.getPhoneNumber());
            namedParameters.addValue(EMAIL_ADDRESS, userQO.getEmailAddress());
            namedParameters.addValue(VEHICLE_NUMBER, userQO.getVehicleNumber());
            namedParameters.addValue(VEHICLE_BRAND, userQO.getVehicleBrand());
            namedParameters.addValue(VEHICLE_MODEL, userQO.getVehicleModel());
            namedParameters.addValue(CREATED_DTS, currentTimeMillis);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            int affectedRowCount = namedParameterJdbcTemplate.update(SQL_CREATE_USER, namedParameters, keyHolder);
            logger.info("Exiting addUser {}", affectedRowCount);

            if (affectedRowCount == 1) {
                return keyHolder.getKey().intValue();
            } else {
                return 0;
            }
        } catch (DuplicateKeyException dke) {
            logger.error("addUser - Duplicate key addition to User DB : {}", dke);
            throw dke;
        } catch (DataAccessException de) {
            logger.error("addUser - Problem adding User to DB : {}", de);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, de.getMessage(), de);
        } catch (Throwable th) {
            logger.error("addUser - Problem adding User to DB : {}", th);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, th.getMessage(), th);
        }
    }

    @Override
    public UserQO getUserByIdForRead(int userId) {
        logger.info("Entering getUserByIdForRead {}", userId);
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource(ID, userId);
            UserQO resp = namedParameterJdbcTemplate.queryForObject(
                    SQL_GET_USER_FOR_ID_FOR_READ,
                    namedParameters, new UserRowMapper());
            logger.info("Exiting getUserByIdForRead {}", userId);
            return resp;
        } catch (EmptyResultDataAccessException e) {
            logger.error("getUserByIdForRead - No User found for id  :  {} ", userId);
            return null;
        } catch (IncorrectResultSizeDataAccessException ie) {
            logger.error("getUserByIdForRead - Problem getting User for id from DB : {}", ie);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, ie.getMessage(), ie);
        } catch (DataAccessException de) {
            logger.error("getUserByIdForRead - Problem getting User for id from DB : {}", de);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, de.getMessage(), de);
        } catch (Throwable th) {
            logger.error("getUserByIdForRead - Problem getting User for id from DB : {}", th);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, th.getMessage(), th);
        }
    }

    @Override
    public UserQO getUserByVehicleNumberForRead(String vehicleNumber) {
        logger.info("Entering getUserByVehicleNumberForRead {}", vehicleNumber);
        try {
            MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            namedParameters.addValue(VEHICLE_NUMBER, vehicleNumber);
            UserQO resp = namedParameterJdbcTemplate.queryForObject(
                    SQL_GET_USER_FOR_VEHICLE_NUMBER_FOR_READ,
                    namedParameters, new UserRowMapper());
            logger.info("Exiting getUserByVehicleNumberForRead {}", vehicleNumber);
            return resp;
        } catch (EmptyResultDataAccessException e) {
            logger.error("getUserByVehicleNumberForRead - No User found for id  :  {} ", vehicleNumber);
            return null;
        } catch (IncorrectResultSizeDataAccessException ie) {
            logger.error("getUserByVehicleNumberForRead - Problem getting User for id from DB : {}", ie);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, ie.getMessage(), ie);
        } catch (DataAccessException de) {
            logger.error("getUserByVehicleNumberForRead - Problem getting User for id from DB : {}", de);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, de.getMessage(), de);
        } catch (Throwable th) {
            logger.error("getUserByVehicleNumberForRead - Problem getting User for id from DB : {}", th);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, th.getMessage(), th);
        }
    }


    @Override
    public List<UserQO> getValidVehicleNumberPresentInList(List<String> vehicleNumberList) {
        logger.info("Entering getValidVehicleNumberPresentInList {}", vehicleNumberList);
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource("vehicleNumberList", vehicleNumberList);
            List<UserQO> resp = namedParameterJdbcTemplate.query(SQL_GET_LIST_OF_VALID_VEHICLE_NUMBER_IN_DATABASE, namedParameters, new UserRowMapper());
            logger.info("Exiting getValidVehicleNumberPresentInList {}", vehicleNumberList);
            return resp;
        } catch (EmptyResultDataAccessException e) {
            logger.error("getValidVehicleNumberPresentInList - No valid license number found in list  :  {} ", vehicleNumberList);
            return null;
        } catch (IncorrectResultSizeDataAccessException ie) {
            logger.error("getValidVehicleNumberPresentInList - Problem getting valid license number from DB : {}", ie);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, ie.getMessage(), ie);
        } catch (DataAccessException de) {
            logger.error("getValidVehicleNumberPresentInList - Problem getting valid license number from DB : {}", de);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, de.getMessage(), de);
        } catch (Throwable th) {
            logger.error("getValidVehicleNumberPresentInList - Problem getting valid license number from DB : {}", th);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, th.getMessage(), th);
        }
    }

    @Override
    public boolean updateUser(UserQO userQO) {
        logger.info("Entering updateUser {}", userQO);
        try {
            MapSqlParameterSource namedParameters = new MapSqlParameterSource();
            namedParameters.addValue(EMAIL_ADDRESS, userQO.getEmailAddress());
            namedParameters.addValue(PHONE_NUMBER, userQO.getPhoneNumber());
            namedParameters.addValue(ID, userQO.getId());
            int affectedRowCount = namedParameterJdbcTemplate.update(SQL_UPDATE_USER, namedParameters);
            logger.info("Exiting updateUser {}", affectedRowCount);
            if (affectedRowCount == 1) {
                return true;
            } else {
                return false;
            }
        } catch (DataAccessException de) {
            logger.error("updateUser - Problem updating User to DB : {}", de);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, de.getMessage(), de);
        } catch (Throwable th) {
            logger.error("updateUser - Problem updating User to DB : {}", th);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, th.getMessage(), th);
        }
    }


}
