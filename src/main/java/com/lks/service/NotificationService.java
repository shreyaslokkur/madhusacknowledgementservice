package com.lks.service;

import com.lks.MASManagementProperties;
import com.lks.connector.TallyConnector;
import com.lks.core.*;
import com.lks.db.dao.NotificationDAO;
import com.lks.db.dao.UserDAO;
import com.lks.db.qo.NotificationQO;
import com.lks.db.qo.UserQO;
import com.lks.error.ErrorHandler;
import com.lks.error.MASErrorSeverity;
import com.lks.error.WarningHandler;
import com.lks.generator.AcknowledgementServiceGenerator;
import com.lks.models.AcknowledgementDTO;
import com.lks.models.TransactionDTO;
import com.lks.parser.DayBookParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 16/4/17
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class NotificationService {

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    private static final String fileName = "static/DayBook.xml";


    @Autowired
    AcknowledgementServiceGenerator acknowledgementServiceGenerator;

    @Autowired
    NotificationDAO notificationDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    DayBookParser dayBookParser;

    @Autowired
    ErrorHandler errorHandler;

    @Autowired
    TallyConnector tallyConnector;

    @Autowired
    MASManagementProperties masManagementProperties;

    @Autowired
    FilterationService filterationService;

    @Autowired
    WarningHandler warningHandler;

    private File tempFile = null;



    public void execute() {

        try{
            //call tally service to get the dayBook.xml
            tempFile = File.createTempFile("DayBook", "xml");
            logger.info("Created the daybook xml");

            tallyConnector.retrieveFromTally(tempFile);
            List<TransactionDTO> transactionDTOList = parseXMLFromTally();
            if(transactionDTOList != null && transactionDTOList.size() > 0) {
                transactionDTOList = filterationService.filter(transactionDTOList);
                logger.info("Number of entries after filteration: {}", transactionDTOList.size());
                persistData(transactionDTOList);
                sendNotification(transactionDTOList);


                List<MASException> exceptionList = MyThreadLocal.get().getExceptionList();
                if( exceptionList != null && exceptionList.size() > 0) {
                    errorHandler.sendErrorEmail(exceptionList);
                }


            }

        } catch (Throwable e) {
            logger.error("Encountered an error during scheduled notification run: {}", e);
            MASException masException = null;
            if(e instanceof MASException) {
                masException = (MASException) e;
            }else {
                masException = new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1,"Encountered an error during scheduled notification run", e);
            }
            List<MASException> exceptionList = new ArrayList<>();
            exceptionList.add(masException);
            errorHandler.sendErrorEmail(exceptionList);
        } finally {
            tempFile = null;
        }


    }

    private List<TransactionDTO> parseXMLFromTally(){
        return dayBookParser.parseDayBook(tempFile);

    }

    /*private List<TransactionDTO> filterResultsFromXML(List<TransactionDTO> transactionDTOList) {
        List<String> vehicleNumberInXML = new ArrayList<>();
        for(TransactionDTO transactionDTO : transactionDTOList) {
            if(transactionDTO.getVehicleNumber() != null) {
                vehicleNumberInXML.add(transactionDTO.getVehicleNumber());
            }
        }
        if(vehicleNumberInXML.size() > 0) {
            List<String> validVehicleNumberPresentInList = userDAO.getValidVehicleNumberPresentInList(vehicleNumberInXML);
            if(validVehicleNumberPresentInList != null) {
                for(String validVehicleNumber : validVehicleNumberPresentInList) {
                    Iterator iter = transactionDTOList.iterator();
                    TransactionDTO transactionDTO;
                    while (iter.hasNext()) {
                        transactionDTO = (TransactionDTO) iter.next();
                        if(validVehicleNumber.equals(transactionDTO.getVehicleNumber())) {
                            iter.remove();
                        }
                    }
                }
            }

            return transactionDTOList;

        } else {
            //TODO: on weekends or holidays no transactions might happen. Need to consider that
            throw new MASException(MASErrorCodes.BAD_INPUT, "No vehicle numbers are present in the Tally Server");
        }


    }*/

    private void persistData(List<TransactionDTO> transactionDTOList) {
        NotificationQO notificationQO = null;
        Iterator iter = transactionDTOList.iterator();
        while (iter.hasNext()) {
            TransactionDTO transactionDTO = (TransactionDTO) iter.next();
            //check if user already exists for that phone number and vehicle number
            //if user exists, get the userId and check if you have to update other details
            //Create a new notification model and add it to db
            notificationQO = new NotificationQO();

            try{
                if(transactionDTO.getPhoneNumber() == null && transactionDTO.getEmailAddress() == null) {
                    logger.error("No details of the users is available: {}", transactionDTO);
                    List<MASWarning> masWarnings = MyThreadLocal.get().getWarningList();
                    if(masWarnings == null) {
                        masWarnings = new ArrayList<>();
                    }
                    masWarnings.add(new MASWarning(MASWarningCodes.NO_EMAIL_PHONENUMBER_PRESENT, "No email or phonenumber users is available: " + transactionDTO));
                }
                UserQO userQO = userDAO.getUserByVehicleNumberForRead(transactionDTO.getVehicleNumber());
                int userId = 0;
                if(userQO != null) {
                    userId = userQO.getId();
                    notificationQO.setRepeat(true);
                    transactionDTO.setRepeat(true);
                    //check if email address and phone number is missing.
                    boolean isAltered = false;
                    if(userQO.getPhoneNumber() == null && transactionDTO.getPhoneNumber() != null) {
                        userQO.setPhoneNumber(transactionDTO.getPhoneNumber());
                        isAltered = true;
                    }
                    if(userQO.getEmailAddress() == null && transactionDTO.getEmailAddress() != null) {
                        userQO.setEmailAddress(transactionDTO.getEmailAddress());
                        isAltered = true;
                    }
                    if(isAltered) {
                        userDAO.updateUser(userQO);
                    }
                }
                else {
                    userQO = new UserQO();
                    userQO.setName(transactionDTO.getName());
                    userQO.setEmailAddress(transactionDTO.getEmailAddress());
                    userQO.setVehicleNumber(transactionDTO.getVehicleNumber());
                    userQO.setPhoneNumber(transactionDTO.getPhoneNumber());
                    userQO.setVehicleBrand(transactionDTO.getVehicleBrandName());
                    userQO.setVehicleModel(transactionDTO.getVehicleModelName());
                    userId = userDAO.addUser(userQO);
                }
                notificationQO.setUserId(userId);
                if(transactionDTO.getServiceType() != null) {
                    notificationQO.setServiceType(transactionDTO.getServiceType().name());
                } else {
                    notificationQO.setServiceType(ServiceType.NOT_KNOWN.name());
                }
                notificationQO.setNotificationStatus(NotificationStatus.NOT_NOTIFIED.name());

                int key = notificationDAO.addNotification(notificationQO);
                transactionDTO.setId(key);



            }catch (Exception e) {
                logger.error("Error storing the user in database: {}", transactionDTO);
                List<MASException> masExceptions = MyThreadLocal.get().getExceptionList();
                if(masExceptions == null) {
                    masExceptions = new ArrayList<>();
                }
                masExceptions.add(new MASException(MASErrorCodes.DATABASE_STORING_ERROR, MASErrorSeverity.LEVEL_1, "Error storing user in database: "+ transactionDTO, e));


            }

        }

    }

    private List<AcknowledgementDTO> sendNotification(List<TransactionDTO> transactionDTOList) {

        List<AcknowledgementDTO> acknowledgementDTOList = null;

        if(masManagementProperties.isUAT()) {
            acknowledgementDTOList = acknowledgementServiceGenerator.generateUATAcknowledgement(transactionDTOList);
        }else {
            acknowledgementDTOList = acknowledgementServiceGenerator.generateAcknowledgement(transactionDTOList);
        }
        return acknowledgementDTOList;
    }


}
