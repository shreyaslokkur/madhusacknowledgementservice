package com.lks.generator;


import com.lks.core.MASErrorCodes;
import com.lks.core.MyThreadLocal;
import com.lks.core.NotificationStatus;
import com.lks.db.dao.NotificationDAO;
import com.lks.db.qo.NotificationQO;
import com.lks.error.ErrorHandler;
import com.lks.core.MASException;
import com.lks.models.AcknowledgementDTO;
import com.lks.models.TransactionDTO;
import com.lks.notifications.EmailNotification;

import com.lks.notifications.SMSNotification;
import com.lks.notifications.UATEmailNotification;
import com.lks.notifications.UATSMSNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


public class AcknowledgementServiceGenerator {

    private static final Logger logger = LoggerFactory.getLogger(AcknowledgementServiceGenerator.class);


    @Autowired
    EmailNotification emailNotification;

    @Autowired
    SMSNotification smsNotification;

    @Autowired
    UATEmailNotification uatEmailNotification;

    @Autowired
    UATSMSNotification uatSmsNotification;

    @Autowired
    ErrorHandler errorHandler;

    @Autowired
    NotificationDAO notificationDAO;

    public List<AcknowledgementDTO> generateUATAcknowledgement(List<TransactionDTO> transactionDTOList) {
        //TODO BUG: transactionDTO list was empty even if the notificaiton status was not sent in UAT
        logger.info("Entering generateAcknowledgement: {}", transactionDTOList);
        List<AcknowledgementDTO> acknowledgementDTOList = new ArrayList<>();
        AcknowledgementDTO acknowledgementDTO;
        for(TransactionDTO transactionDTO : transactionDTOList) {
            acknowledgementDTO = new AcknowledgementDTO();

            logger.info("Processing the user: {}", transactionDTO);
            acknowledgementDTO.setNotificationId(transactionDTO.getId());
            try{
                    //send email notification
                logger.info("Sending email to the user: {}", transactionDTO.getEmailAddress());
                uatEmailNotification.generateAndSendEmail(transactionDTO);
                acknowledgementDTO.setEmailNotificationSent(true);
                /*if(transactionDTO.getEmailAddress() != null) {
                    logger.info("Sending email to the user: {}", transactionDTO.getEmailAddress());
                    uatEmailNotification.generateAndSendEmail(transactionDTO);
                    acknowledgementDTO.setEmailNotificationSent(true);
                } else {
                    throw new MASException(MASErrorCodes.EMAIL_NOTIFICATION_ERROR, "User email address has not been entered");
                }*/

            }catch (MASException e){
                logger.error("Encountered exception sending notification to the user: {}. Exception is: {}", transactionDTO, e.getMessage());
                List<MASException> masExceptions = MyThreadLocal.get().getExceptionList();
                if(masExceptions == null) {
                    masExceptions = new ArrayList<>();
                }
                masExceptions.add(e);
                acknowledgementDTO.setEmailNotificationSent(false);
            }
            if(acknowledgementDTO.isEmailNotificationSent()) {
                transactionDTO.setNotificationStatus(NotificationStatus.UAT_NOTIFIED);
            } else {
                transactionDTO.setNotificationStatus(NotificationStatus.NOT_NOTIFIED);
            }
            notificationDAO.updateNotificationStatus(transactionDTO.getId(), transactionDTO.getNotificationStatus().toString());
            acknowledgementDTOList.add(acknowledgementDTO);

        }
        /*try{
            //send sms notification
            logger.info("Sending sms to the admin");
            uatSmsNotification.sendSms(transactionDTOList.size());

        }catch (MASException e){
            logger.error("Encountered exception sending sms notification . Exception is: {}", e.getMessage());
            List<MASException> masExceptions = MyThreadLocal.get().getExceptionList();
            if(masExceptions == null) {
                masExceptions = new ArrayList<>();
            }
            masExceptions.add(e);
        }*/
        logger.info("Exiting generateAcknowledgement");
        return acknowledgementDTOList;
    }


    public List<AcknowledgementDTO> generateAcknowledgement(List<TransactionDTO> transactionDTOList) {
        logger.info("Entering generateAcknowledgement: {}", transactionDTOList);
        List<AcknowledgementDTO> acknowledgementDTOList = new ArrayList<>();
        AcknowledgementDTO acknowledgementDTO;
        for(TransactionDTO transactionDTO : transactionDTOList) {
            acknowledgementDTO = new AcknowledgementDTO();

            logger.info("Processing the user: {}", transactionDTO);
            acknowledgementDTO.setNotificationId(transactionDTO.getId());
            //send notification to that user on sms and email
            try{
                if(transactionDTO.getEmailAddress() != null) {
                    //send email notification
                    logger.info("Sending email to the user: {}", transactionDTO.getEmailAddress());
                    emailNotification.generateAndSendEmail(transactionDTO);
                    acknowledgementDTO.setEmailNotificationSent(true);
                }
            }catch (MASException e){
                logger.error("Encountered exception sending notification to the user: {}. Exception is: {}", transactionDTO, e.getMessage());
                List<MASException> masExceptions = MyThreadLocal.get().getExceptionList();
                if(masExceptions == null) {
                    masExceptions = new ArrayList<>();
                }
                masExceptions.add(e);
                acknowledgementDTO.setEmailNotificationSent(false);
            }
            try{
                if(transactionDTO.getPhoneNumber() != null) {
                    //send sms notification
                    logger.info("Sending sms to the user: {}", transactionDTO.getPhoneNumber());
                    smsNotification.sendSms(transactionDTO);
                    acknowledgementDTO.setSMSNotificationSent(true);
                }
            }catch (MASException e){
                logger.error("Encountered exception sending notification to the user: {}. Exception is: {}", transactionDTO, e.getMessage());
                List<MASException> masExceptions = MyThreadLocal.get().getExceptionList();
                if(masExceptions == null) {
                    masExceptions = new ArrayList<>();
                }
                masExceptions.add(e);
                acknowledgementDTO.setSMSNotificationSent(false);
            }
            acknowledgementDTOList.add(acknowledgementDTO);

            //update notification in database
            if(acknowledgementDTO.isEmailNotificationSent() && acknowledgementDTO.isSMSNotificationSent()) {
                transactionDTO.setNotificationStatus(NotificationStatus.NOTIFIED_BY_SMS_EMAIL);
            } else if(acknowledgementDTO.isEmailNotificationSent()) {
                transactionDTO.setNotificationStatus(NotificationStatus.NOTIFIED_BY_EMAIL);
            } else if(acknowledgementDTO.isSMSNotificationSent()) {
                transactionDTO.setNotificationStatus(NotificationStatus.NOTIFIED_BY_SMS);
            } else {
                transactionDTO.setNotificationStatus(NotificationStatus.NOT_NOTIFIED);
            }

            notificationDAO.updateNotificationStatus(transactionDTO.getId(), transactionDTO.getNotificationStatus().toString());

        }
        logger.info("Exiting generateAcknowledgement");
        return acknowledgementDTOList;
    }



}
