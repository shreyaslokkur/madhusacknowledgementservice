package com.lks.error;

import com.lks.MASManagementProperties;
import com.lks.core.MASException;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by lokkur on 21/10/16.
 */
public class ErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);

    @Autowired
    private MASManagementProperties masManagementProperties;

    private String templateHeaderText = "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Error Message : ${errorMessage}</span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Error Code : ${errorCode}</span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Error Time : ${errorTime}</span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Error StackTrace : </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">${errorStackTrace}</span></p>" ;




    public void sendErrorEmail(List<MASException> masExceptions){
        logger.info("Entering sending error email");

        String resolvedMailBody = generateEmailBody(masExceptions);
        if(resolvedMailBody != null && resolvedMailBody.length() > 0) {
            String emailSubject = masManagementProperties.getShopName() + " Error Message";


            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", masManagementProperties.getEmailHost());
            props.put("mail.smtp.port", masManagementProperties.getEmailPort());

            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(masManagementProperties.getEmailUsername(), masManagementProperties.getEmailPassword());
                        }
                    });

            try {

                // Create a default MimeMessage object.
                Message message = new MimeMessage(session);

                // Set From: header field of the header.
                message.setFrom(new InternetAddress(masManagementProperties.getEmailUsername(), masManagementProperties.getShopName()));

                // Set To: header field of the header.
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(masManagementProperties.getErrorEmailUsername()));

                // Set Subject: header field
                message.setSubject(emailSubject);

                // Create the message part
                MimeBodyPart messageBodyPart = new MimeBodyPart();

                // Now set the actual message
                messageBodyPart.setContent(resolvedMailBody,"text/html; charset=utf-8");

                // Create a multipart message
                Multipart multipart = new MimeMultipart();

                // Set text message part
                multipart.addBodyPart(messageBodyPart);

                // Send the complete message parts
                message.setContent(multipart);
                Transport.send(message);



            } catch (MessagingException e) {
                logger.error("God help you now: {}", e.getMessage());
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                logger.error("God help you now: {}", e.getMessage());
                e.printStackTrace();
            }
            logger.error("Error email has been sent");


        } else {
            logger.info("No LEVEL1 error's encountered");
        }
        logger.info("Exiting Error Email Notification");



    }

    private String generateEmailBody(List<MASException> masExceptionList) {
        StringBuilder errorReport = new StringBuilder();
        Map<String, String> valuesMap = null;
        for(MASException masException : masExceptionList) {
            if(masException.getErrorSeverity().equals(MASErrorSeverity.LEVEL_1)) {
                valuesMap = new HashMap<>();

                valuesMap.put("errorMessage", masException.getMessage());
                valuesMap.put("errorCode", masException.getErrorCodes().toString());
                valuesMap.put("errorTime", getCurrentDateTime());
                valuesMap.put("errorStackTrace", getStackTrace(masException));
                StrSubstitutor sub = new StrSubstitutor(valuesMap);

                String resolvedString = sub.replace(templateHeaderText);

                logger.info("Error Message after resolving template is {}", resolvedString);
                errorReport.append(resolvedString);
            }

        }
        logger.info("The email body is generated");
        return errorReport.toString();

    }

    private String getStackTrace(Throwable t) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        t.printStackTrace(printWriter);
        return result.toString();
    }

    private String getCurrentDateTime(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }
}
