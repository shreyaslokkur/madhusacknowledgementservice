package com.lks.error;

import com.lks.MASManagementProperties;
import com.lks.core.MASWarning;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by lokkur on 21/10/16.
 */
public class WarningHandler {

    private static final Logger logger = LoggerFactory.getLogger(WarningHandler.class);

    @Autowired
    private MASManagementProperties masManagementProperties;

    private String templateHeaderText = "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Warning Message : ${warningMessage}</span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Warning Code : ${warningCode}</span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Warning Time : ${warningTime}</span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Warning StackTrace : </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">${warningStackTrace}</span></p>" ;




    public void sendWarningEmail(List<MASWarning> masWarnings){
        logger.info("Entering sending warning email");

        String resolvedMailBody = generateEmailBody(masWarnings);

        String emailSubject = masManagementProperties.getShopName() + " Warning Message";


        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", masManagementProperties.getEmailHost());
        props.put("mail.smtp.port", masManagementProperties.getEmailPort());

        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(masManagementProperties.getEmailUsername(), masManagementProperties.getEmailPassword());
                    }
                });

        try {

            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(masManagementProperties.getEmailUsername(), masManagementProperties.getShopName()));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(masManagementProperties.getErrorEmailUsername()));

            // Set Subject: header field
            message.setSubject(emailSubject);

            // Create the message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setContent(resolvedMailBody,"text/html; charset=utf-8");

            // Create a multipart message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);
            Transport.send(message);

            logger.info("Exiting Warning Email Notification");

        } catch (MessagingException e) {
            logger.error("God help you now: {}", e.getMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            logger.error("God help you now: {}", e.getMessage());
            e.printStackTrace();
        }


    }

    private String generateEmailBody(List<MASWarning> masWarningList) {
        StringBuilder warningReport = new StringBuilder();
        Map<String, String> valuesMap = null;
        for(MASWarning masWarning : masWarningList) {
            valuesMap = new HashMap<>();

            valuesMap.put("warningMessage", masWarning.getMessage());
            valuesMap.put("warningCode", masWarning.getWarningCodes().toString());
            valuesMap.put("warningTime", getCurrentDateTime());
            valuesMap.put("warningStackTrace", getStackTrace(masWarning));
            StrSubstitutor sub = new StrSubstitutor(valuesMap);

            String resolvedString = sub.replace(templateHeaderText);

            logger.info("Warning Message after resolving template is {}", resolvedString);
            warningReport.append(resolvedString);

        }
        logger.info("The email body: {}", warningReport.toString());
        return warningReport.toString();

    }

    private String getStackTrace(Throwable t) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        t.printStackTrace(printWriter);
        return result.toString();
    }

    private String getCurrentDateTime(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }
}
