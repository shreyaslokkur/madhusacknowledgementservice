package com.lks.error;

/**
 * Created by lokkur on 18/10/16.
 */
public enum MASErrorSeverity {

    LEVEL_1,
    LEVEL_2,
    LEVEL_3;
}
