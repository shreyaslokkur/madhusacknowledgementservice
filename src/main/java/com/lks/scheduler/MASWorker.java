
package com.lks.scheduler;


import com.lks.admin.AdminService;
import com.lks.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

public class MASWorker {

    private static final Logger logger = LoggerFactory.getLogger(MASWorker.class);

    @Autowired
    NotificationService notificationService;

    @Autowired
    AdminService adminService;



    @Scheduled(cron = "0 0/10 * * * ?")
    public void notificationWorker(){

        logger.info("Inside the notification scheduled job");
        notificationService.execute();

    }

    @Scheduled(cron = "0 15 18 1/1 * ?", zone = "Asia/Colombo")
    public void adminWorker(){

        logger.info("Inside the admin scheduled");
        adminService.execute();

    }





}
