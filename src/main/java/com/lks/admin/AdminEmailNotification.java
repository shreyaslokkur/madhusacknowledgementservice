package com.lks.admin;

import com.lks.MASManagementProperties;
import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.error.MASErrorSeverity;
import com.lks.models.SMSBalance;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * To change this template use File | Settings | File Templates.
 */
public class AdminEmailNotification {

    private static final Logger logger = LoggerFactory.getLogger(AdminEmailNotification.class);

    @Autowired
    private MASManagementProperties masManagementProperties;

    private String templateHeaderText = " <p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">Notification Service report for ${todayDate} from ${shopName} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">1. Number of SMS only notifications successfully sent : ${smsOnlySuccessfullNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">2. Number of Email only notifications successfully sent : ${emailOnlySuccessfullNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">3. Number of SMS and Email notifications successfully sent : ${successfullNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">4. Number of notifications which were unsuccessfull : ${unsuccessfullNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">5. Number of records where vehicle number was missing : ${vehicleNumberMissingNotification} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">6. Number of records where customer name was missing : ${nameMissingNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">7. Number of records where phone number was missing : ${phoneNumberMissingNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">8. Number of records where email address was missing : ${emailAddressMissingNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">9. Number of records where Vehicle Brand name was missing : ${vehicleBrandNameMissingNotifications} </span></p>" +
            "<p><span style=\"color:#646464;font-size:20px;font-family:arial;font-style:normal\">10. SMS Balance : ${smsBalance} </span></p>" ;


    public void generateAndSendEmail(AdminModel adminModel,String filename) {

        logger.info("Entering Admin Email Notification");

        String resolvedMailBody = generateEmailBody(adminModel);


        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", masManagementProperties.getEmailHost());
        props.put("mail.smtp.port", masManagementProperties.getEmailPort());

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(masManagementProperties.getEmailUsername(), masManagementProperties.getEmailPassword());
                    }
                });

        try {

            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(masManagementProperties.getEmailUsername(), masManagementProperties.getShopName()));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(masManagementProperties.getAdminEmail()));

            // Set BCC: header field of the header.
            message.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(masManagementProperties.getNotificationReportEmail()));

            // Set BCC: header field of the header.
            message.setRecipients(Message.RecipientType.BCC,
                    InternetAddress.parse(masManagementProperties.getUatEmail()));

            // Set Subject: header field
            message.setSubject("Notification Service Day Report");

            // Create the message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setContent(resolvedMailBody,"text/html; charset=utf-8");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);



            // Part two is attachment
            MimeBodyPart attachementBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(filename);
            attachementBodyPart.setDataHandler(new DataHandler(source));
            attachementBodyPart.setFileName(filename);
            multipart.addBodyPart(attachementBodyPart);

            // Send the complete message parts
            message.setContent(multipart);
            Transport.send(message);

            logger.info("Exiting Admin Email Notification");

        } catch (MessagingException e) {
            logger.error("Unable to send Admin email: {}", e);
            throw new MASException(MASErrorCodes.EMAIL_NOTIFICATION_ERROR, MASErrorSeverity.LEVEL_2,"Unable to send email to the admin ",e);
        } catch (UnsupportedEncodingException e) {
            logger.error("Unable to send Admin email : {} ", e);
            throw new MASException(MASErrorCodes.EMAIL_NOTIFICATION_ERROR, MASErrorSeverity.LEVEL_2, "Unable to send email to the admin ",e);
        }


    }




    private String generateEmailBody(AdminModel adminModel) {
        Map<String, String> valuesMap = new HashMap<>();
        String resolvedString = null;
        //getTodaysDate

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy");
        LocalDate localDate = LocalDate.now();
        valuesMap.put("todayDate", dtf.format(localDate));
        valuesMap.put("shopName", masManagementProperties.getShopName());
        valuesMap.put("smsOnlySuccessfullNotifications", Integer.toString(adminModel.getSuccessfullSMSOnlyList().size()));
        valuesMap.put("emailOnlySuccessfullNotifications", Integer.toString(adminModel.getSuccessfullEmailOnlyList().size()));
        valuesMap.put("successfullNotifications", Integer.toString(adminModel.getSuccessfullEmailAndSMSList().size()));
        valuesMap.put("unsuccessfullNotifications", Integer.toString(adminModel.getUnsuccessfullList().size()));
        valuesMap.put("vehicleNumberMissingNotification", Integer.toString(adminModel.getVehicleNumberMissingList().size()));
        valuesMap.put("nameMissingNotifications", Integer.toString(adminModel.getNameMissingList().size()));
        valuesMap.put("phoneNumberMissingNotifications", Integer.toString(adminModel.getPhoneNumberMissingList().size()));
        valuesMap.put("emailAddressMissingNotifications", Integer.toString(adminModel.getEmailAddressMissingList().size()));
        valuesMap.put("vehicleBrandNameMissingNotifications", Integer.toString(adminModel.getVehicleBrandMissingList().size()));
        if(adminModel.getSmsBalance() != null) {
            SMSBalance smsBalance = adminModel.getSmsBalance();
            if(smsBalance.getBalance() != null) {
                valuesMap.put("smsBalance", adminModel.getSmsBalance().getBalance().getSms());
            }
        } else {
            valuesMap.put("smsBalance", "Unable to retrieve it from Text Local API");

        }

        StrSubstitutor sub = new StrSubstitutor(valuesMap);

        resolvedString = sub.replace(templateHeaderText);

        logger.info("The email body: {}", resolvedString);
        return resolvedString ;
    }


}
