package com.lks.admin;

import com.lks.core.*;
import com.lks.db.dao.NotificationDAO;
import com.lks.db.dao.UserDAO;
import com.lks.db.qo.NotificationQO;
import com.lks.db.qo.UserQO;
import com.lks.error.ErrorHandler;
import com.lks.error.MASErrorSeverity;
import com.lks.models.SMSBalance;
import com.lks.models.TransactionDTO;
import com.lks.notifications.SMSBalanceNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 16/4/17
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminService {

    private static long twentyFourHoursInMills = 86400000;

    private static Logger logger = LoggerFactory.getLogger(AdminService.class);

    @Autowired
    NotificationDAO notificationDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    ExcelGenerator excelGenerator;

    @Autowired
    AdminEmailNotification adminEmailNotification;

    @Autowired
    ErrorHandler errorHandler;

    @Autowired
    SMSBalanceNotification smsBalanceNotification;

    private File tempFile = null;
    

    public void execute() {
        // retrieve all the records of the day from database

        logger.info("Entering Admin Service execute");

        try{
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy");
            LocalDate localDate = LocalDate.now();
            tempFile = File.createTempFile(dtf.format(localDate), ".xls");

            logger.info("Created the temp file: {}", tempFile.getName());

            List<TransactionDTO> recordsOfDayFromDatabase = getRecordsOfDayFromDatabase();

            //sort out the successfull ones from unsuccessfull

            //sort out ones which dont have certain fields

            AdminModel adminModel = sortNotifications(recordsOfDayFromDatabase);

            SMSBalance smsBalance = smsBalanceNotification.checkBalance();
            adminModel.setSmsBalance(smsBalance);

            //create excel for all

            createExcel(adminModel);

            //send email with attachment
            sendEmail(adminModel, tempFile);

        } catch (Throwable e) {
            MASException masException = null;
            if(e instanceof MASException) {
                masException = (MASException) e;
            }else {
                masException = new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_1, "Encountered an error during scheduled notification run", e);
            }
            List<MASException> exceptionList = new ArrayList<>();
            exceptionList.add(masException);
            errorHandler.sendErrorEmail(exceptionList);
        } finally {
            tempFile = null;
        }


    }

    private List<TransactionDTO> getRecordsOfDayFromDatabase() {
        logger.info("Entered the method to getRecordsOfDayFromDatabase");
        List<TransactionDTO> transactionDTOList = new ArrayList<>();
        long currentTimeInMills = System.currentTimeMillis();
        long timeInMillsTwentyFourHoursBack =    currentTimeInMills - twentyFourHoursInMills;
        List<NotificationQO> notificationByCreatedDtsRange = notificationDAO.getNotificationByCreatedDtsRange(timeInMillsTwentyFourHoursBack, currentTimeInMills);
        logger.info("Number of records present in the date range {} {} is {}", timeInMillsTwentyFourHoursBack, currentTimeInMills, notificationByCreatedDtsRange.size());
        List<Integer> userIdList = new ArrayList<>();
        UserQO userQO = null;
        for(NotificationQO notificationQO : notificationByCreatedDtsRange) {
            userQO = userDAO.getUserByIdForRead(notificationQO.getUserId());
            transactionDTOList.add(createTransactionDTOObject(notificationQO, userQO));
        }

        logger.info("Number of transaction objects created: {}", transactionDTOList.size());
        return transactionDTOList;
    }

    private TransactionDTO createTransactionDTOObject(NotificationQO notificationQO, UserQO userQO) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setName(userQO.getName());
        transactionDTO.setPhoneNumber(userQO.getPhoneNumber());
        transactionDTO.setVehicleNumber(userQO.getVehicleNumber());
        transactionDTO.setEmailAddress(userQO.getEmailAddress());
        transactionDTO.setOdometerReading(notificationQO.getOdometerReading());
        transactionDTO.setServiceType(ServiceType.valueOf(notificationQO.getServiceType()));
        transactionDTO.setRepeat(notificationQO.isRepeat());
        transactionDTO.setNotificationStatus(NotificationStatus.valueOf(notificationQO.getNotificationStatus()));
        transactionDTO.setVehicleModelName(userQO.getVehicleModel());
        transactionDTO.setVehicleBrandName(userQO.getVehicleBrand());
        return transactionDTO;
    }

    private AdminModel sortNotifications(List<TransactionDTO> transactionDTOList) {

        AdminModel adminModel = new AdminModel();

        for(TransactionDTO transactionDTO : transactionDTOList) {
            logger.info("Transaction Object to Admin model : {}", transactionDTO);
            if(transactionDTO.getVehicleNumber() == null) {
                adminModel.getVehicleNumberMissingList().add(transactionDTO);
            }
            if(transactionDTO.getName() == null) {
                adminModel.getNameMissingList().add(transactionDTO);
            }
            if(transactionDTO.getVehicleBrandName() == null) {
                adminModel.getVehicleBrandMissingList().add(transactionDTO);
            }
            if(NotificationStatus.UAT_NOTIFIED.equals(transactionDTO.getNotificationStatus())) {
                if(transactionDTO.getEmailAddress() == null) {
                    adminModel.getEmailAddressMissingList().add(transactionDTO);
                }
                if(transactionDTO.getPhoneNumber() == null) {
                    adminModel.getPhoneNumberMissingList().add(transactionDTO);
                }
            }
            else{
                if(NotificationStatus.NOTIFIED_BY_EMAIL.equals(transactionDTO.getNotificationStatus())) {
                    adminModel.getSuccessfullEmailOnlyList().add(transactionDTO);
                    if(transactionDTO.getPhoneNumber() == null) {
                        adminModel.getPhoneNumberMissingList().add(transactionDTO);
                    }
                } else if(NotificationStatus.NOTIFIED_BY_SMS.equals(transactionDTO.getNotificationStatus())) {
                    adminModel.getSuccessfullSMSOnlyList().add(transactionDTO);
                    if(transactionDTO.getEmailAddress() == null) {
                        adminModel.getEmailAddressMissingList().add(transactionDTO);
                    }
                } else if(NotificationStatus.NOTIFIED_BY_SMS_EMAIL.equals(transactionDTO.getNotificationStatus())) {
                    adminModel.getSuccessfullEmailAndSMSList().add(transactionDTO);
                } else if(NotificationStatus.NOT_NOTIFIED.equals(transactionDTO.getNotificationStatus())) {
                    adminModel.getUnsuccessfullList().add(transactionDTO);
                    if(transactionDTO.getEmailAddress() == null) {
                        adminModel.getEmailAddressMissingList().add(transactionDTO);
                    }
                    if(transactionDTO.getPhoneNumber() == null) {
                        adminModel.getPhoneNumberMissingList().add(transactionDTO);
                    }

                }
            }
        }
        return adminModel;
    }

    private void createExcel(AdminModel adminModel){
        excelGenerator.generateExcel(adminModel, tempFile);
    }

    private void sendEmail(AdminModel adminModel,File attachment) {
        adminEmailNotification.generateAndSendEmail(adminModel, attachment.getAbsolutePath());

    }

}
