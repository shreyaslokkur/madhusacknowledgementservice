package com.lks.admin;

import com.lks.models.SMSBalance;
import com.lks.models.TransactionDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 16/4/17
 * Time: 5:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminModel {

    List<TransactionDTO> successfullSMSOnlyList = new ArrayList<>();
    List<TransactionDTO> successfullEmailOnlyList = new ArrayList<>();
    List<TransactionDTO> successfullEmailAndSMSList = new ArrayList<>();
    List<TransactionDTO> unsuccessfullList = new ArrayList<>();
    List<TransactionDTO> emailAddressMissingList = new ArrayList<>();
    List<TransactionDTO> phoneNumberMissingList = new ArrayList<>();
    List<TransactionDTO> vehicleNumberMissingList = new ArrayList<>();
    List<TransactionDTO> nameMissingList = new ArrayList<>();
    List<TransactionDTO> vehicleBrandMissingList = new ArrayList<>();
    SMSBalance smsBalance;

    public List<TransactionDTO> getSuccessfullSMSOnlyList() {
        return successfullSMSOnlyList;
    }


    public List<TransactionDTO> getSuccessfullEmailOnlyList() {
        return successfullEmailOnlyList;
    }


    public List<TransactionDTO> getSuccessfullEmailAndSMSList() {
        return successfullEmailAndSMSList;
    }


    public List<TransactionDTO> getUnsuccessfullList() {
        return unsuccessfullList;
    }


    public List<TransactionDTO> getEmailAddressMissingList() {
        return emailAddressMissingList;
    }


    public List<TransactionDTO> getPhoneNumberMissingList() {
        return phoneNumberMissingList;
    }


    public List<TransactionDTO> getVehicleNumberMissingList() {
        return vehicleNumberMissingList;
    }


    public List<TransactionDTO> getNameMissingList() {
        return nameMissingList;
    }

    public List<TransactionDTO> getVehicleBrandMissingList() {
        return vehicleBrandMissingList;
    }

    public SMSBalance getSmsBalance() {
        return smsBalance;
    }

    public void setSmsBalance(SMSBalance smsBalance) {
        this.smsBalance = smsBalance;
    }
}
