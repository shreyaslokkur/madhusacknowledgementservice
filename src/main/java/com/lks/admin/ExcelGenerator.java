package com.lks.admin;

import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.error.MASErrorSeverity;
import com.lks.models.TransactionDTO;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 24/6/16
 * Time: 7:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExcelGenerator {

    private static final Logger logger = LoggerFactory.getLogger(ExcelGenerator.class);


    private int cellNum;
    //Create blank workbook
    private XSSFWorkbook workbook = new XSSFWorkbook();

    private void setCellValue(Cell cell, Object obj) {
        if(obj instanceof String) {
            cell.setCellValue((String)obj);
        }else if(obj instanceof Integer) {
            cell.setCellValue((Integer)obj);
        }

    }

    private void createCell(Object obj, XSSFSheet spreadSheet, XSSFRow row) {
        Cell nameCell = row.createCell(cellNum);
        setCellValue(nameCell, obj);
        spreadSheet.autoSizeColumn(cellNum);
        cellNum++;
    }

    private void createSpreadSheet(String sheetName, List<TransactionDTO> transactionDTOList) {
        XSSFSheet spreadsheet = workbook.createSheet(sheetName);
        createHeader(spreadsheet);
        //Create row object
        XSSFRow row;


        XSSFFont defaultFont= workbook.createFont();
        defaultFont.setFontHeightInPoints((short)10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(false);
        defaultFont.setItalic(false);

        //Iterate over data and write to sheet
        int rowid = 1;

        for (TransactionDTO transactionDTO : transactionDTOList)
        {
            row = spreadsheet.createRow(rowid++);
            cellNum = 0;
            createCell(transactionDTO.getName(), spreadsheet, row);
            createCell(transactionDTO.getPhoneNumber(), spreadsheet, row);
            createCell(transactionDTO.getEmailAddress(), spreadsheet, row);
            createCell(transactionDTO.getVehicleNumber(), spreadsheet, row);
        }

    }


    public void generateExcel(AdminModel adminModel, File tempFile) {
        logger.info("Entered the method to generate excel: {}", tempFile.getAbsolutePath());

        //Create spreadsheets for all the different lists
        createSpreadSheet("Successful Notifications", adminModel.getSuccessfullEmailAndSMSList());
        createSpreadSheet("SMS Only Notifications", adminModel.getSuccessfullSMSOnlyList());
        createSpreadSheet("Email Only Notifications", adminModel.getSuccessfullEmailOnlyList());
        createSpreadSheet("Vehicle Number Missing", adminModel.getVehicleNumberMissingList());
        createSpreadSheet("Name Missing", adminModel.getNameMissingList());
        createSpreadSheet("Phone Number Missing", adminModel.getPhoneNumberMissingList());
        createSpreadSheet("Email Address Missing", adminModel.getEmailAddressMissingList());
        createSpreadSheet("Vehicle Brand Name Missing", adminModel.getVehicleBrandMissingList());


        //Write the workbook in file system

        writeToFile(tempFile);


    }

    private void writeToFile(File tempFile) {
        logger.info("Begin writing to the excel file: {}", tempFile.getAbsolutePath());
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(tempFile);
            workbook.write(out);
            out.close();

        } catch (FileNotFoundException e) {
            logger.error("Unable to find the temp excel file: {}", e);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_2,"Admin temp excel file not found", e);
        } catch (IOException e) {
            logger.error("Encountered exception when writing to the excel file: {}", e);
            throw new MASException(MASErrorCodes.INTERNAL_SERVER_ERROR, MASErrorSeverity.LEVEL_2, "Unable to write to temp excel file", e);
        }

    }

    private void createHeader(XSSFSheet spreadSheet){

        XSSFRow row = spreadSheet.createRow(0);
        CellStyle style=spreadSheet.getWorkbook().createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(getFont(spreadSheet.getWorkbook()));
        String[] headers = {"Name", "Phone Number", "Email Address","Vehicle Number"};
        int cellId = 0;
        for(String headerName : headers) {
            Cell cell = row.createCell(cellId++);
            cell.setCellValue(headerName);
            cell.setCellStyle(style);
        }




    }


    private XSSFFont getFont(XSSFWorkbook workbook){
        XSSFFont font= workbook.createFont();
        font.setFontHeightInPoints((short)10);
        font.setFontName("Arial");
        font.setColor(IndexedColors.BLACK.getIndex());
        font.setBold(true);
        font.setItalic(false);
        return font;
    }





}
