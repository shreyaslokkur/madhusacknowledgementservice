package com.lks.connector;

import com.lks.MASManagementProperties;
import com.lks.core.MASErrorCodes;
import com.lks.core.MASException;
import com.lks.error.MASErrorSeverity;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
/**
 * Created by lokkur on 26/04/17.
 */
public class TallyConnector {

    private static final Logger logger = LoggerFactory.getLogger(TallyConnector.class);

    @Autowired
    private MASManagementProperties masManagementProperties;

    private String createRequest(String fromDate, String toDate, String companyName)
    {
        logger.info("Entered the method to create txml request {} {}", fromDate, toDate);

        String txml = "<ENVELOPE>"
                + "<HEADER>"
                + "<VERSION>1</VERSION>"
                + "<TALLYREQUEST>EXPORT</TALLYREQUEST>"
                + "<TYPE>DATA</TYPE>"
                + "<ID>DAY BOOK</ID>"
                + "</HEADER>"
                + "<BODY>"
                + "<DESC>"
                + "<STATICVARIABLES>"
                + "<EXPLODEFLAG>Yes</EXPLODEFLAG>"
                + "<SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT>"
                + "<SVFROMDATE>{0}</SVFROMDATE>"
                + "<SVTODATE>{1}</SVTODATE>"
                + "<SVCURRENTCOMPANY>{2}</SVCURRENTCOMPANY>"
                + "</STATICVARIABLES>"
                + "</DESC>"
                + "</BODY>"
                + "</ENVELOPE>";
        String replacedText = MessageFormat.format(txml, fromDate, toDate, companyName);
        logger.info("The final txml is {}", replacedText);
        return replacedText;
    }

    public void retrieveFromTally(File tempFile) {
        logger.info("Entering the method to retrieve data from tally");
        try{
            String tallyURL = masManagementProperties.getTallyURL();

            logger.info("The tally URL is: {}", tallyURL);

            String soapAction = "";

            LocalDate toJodaDate = new LocalDate();

            String toDate = toJodaDate.toString("dd/MM/yyyy");

            LocalDate fromJodaDate = toJodaDate.minusDays(1);

            String fromDate = fromJodaDate.toString("dd/MM/yyyy");

            String tallyCompanyName = masManagementProperties.getTallyCompanyName();

            String voucher = createRequest(fromDate, toDate, tallyCompanyName);


            // Create the connection
            logger.info("Creating connection to tally");
            URL url = new URL(tallyURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;


            ByteArrayInputStream bin = new ByteArrayInputStream(voucher.getBytes());
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

// Copy the SOAP file to the open connection.

            copy(bin, bout);

            byte[] b = bout.toByteArray();

// Set the appropriate HTTP parameters.
            httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/html");
            httpConn.setRequestProperty("SOAPAction", soapAction);
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);

            httpConn.setConnectTimeout(15000);
// Everything's set up; send the XML that was read in to b.
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

// Read the response and write it to standard out.

            Document document = readXml(httpConn.getInputStream());

            writeToDayParserFile(document, tempFile);



        }catch (SocketTimeoutException e) {
            logger.error("Socket Timeout Exception connecting to Tally: {}", e);
            throw new MASException(MASErrorCodes.TALLY_CONNECTOR_ERROR, MASErrorSeverity.LEVEL_1, "Error connecting to Tally Server", e);
        }catch (Exception e) {
            logger.error("Tally Read error: {}", e);
            throw new MASException(MASErrorCodes.TALLY_READ_ERROR, MASErrorSeverity.LEVEL_1, "Error while reading from tally", e);
        }


    }

    private void copy(InputStream in, OutputStream out)
            throws IOException {


                byte[] buffer = new byte[256];
                while (true) {
                    int bytesRead = in.read(buffer);
                    if (bytesRead == -1) {
                        break;
                    }
                    out.write(buffer, 0, bytesRead);
                }

    }

    private Document readXml(InputStream is) throws SAXException, IOException,
            ParserConfigurationException {

        logger.info("Entering method to read the tally response xml");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setValidating(false);
        dbf.setIgnoringComments(false);
        dbf.setIgnoringElementContentWhitespace(true);
        dbf.setNamespaceAware(true);

        DocumentBuilder db = null;
        db = dbf.newDocumentBuilder();
        db.setEntityResolver(new NullResolver());


        return db.parse(is);
    }

    private void writeToDayParserFile(Document doc, File file) throws IOException, TransformerException {
        logger.info("Writing the document to a day parser file");
        DOMSource source = new DOMSource(doc);

        FileWriter writer = new FileWriter(file);
        StreamResult result = new StreamResult(writer);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.transform(source, result);
    }

    private class NullResolver implements EntityResolver {
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException,
                IOException {
            return new InputSource(new StringReader(""));
        }
    }

}
