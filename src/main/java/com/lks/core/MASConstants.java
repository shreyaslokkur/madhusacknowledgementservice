package com.lks.core;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 12/4/17
 * Time: 8:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class MASConstants {

    public static final String BASICBUYERADDRESSLIST = "BASICBUYERADDRESS.LIST";
    public static final String BASICBUYERADDRESS = "BASICBUYERADDRESS";
    public static final String BASICBUYERNAME = "BASICBUYERNAME";
    public static final String VOUCHER = "VOUCHER";
    public static final String UUID = "UUID";
}
