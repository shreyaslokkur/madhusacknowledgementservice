package com.lks.core;

import com.lks.error.MASErrorSeverity;

public class MASException extends RuntimeException {

    private String message;
    private MASErrorCodes errorCodes;
    private MASErrorSeverity errorSeverity;


    public MASException(MASErrorCodes errorCodes, MASErrorSeverity errorSeverity, String message) {
        super(message);
        this.message = message;
        this.errorSeverity = errorSeverity;
        this.errorCodes = errorCodes;
    }

    public MASException(MASErrorCodes masErrorCodes, MASErrorSeverity errorSeverity, String message, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.errorSeverity = errorSeverity;
        this.errorCodes = masErrorCodes;
    }

    public MASException(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MASErrorCodes getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(MASErrorCodes errorCodes) {
        this.errorCodes = errorCodes;
    }

    public MASErrorSeverity getErrorSeverity() {
        return errorSeverity;
    }

    public void setErrorSeverity(MASErrorSeverity errorSeverity) {
        this.errorSeverity = errorSeverity;
    }
}
