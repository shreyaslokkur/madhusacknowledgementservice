package com.lks.core;

/**
 * Created by lokkur on 31/03/17.
 */
public enum NotificationStatus {
    NOTIFIED_BY_SMS_EMAIL,
    NOTIFIED_BY_SMS,
    NOTIFIED_BY_EMAIL,
    NOT_NOTIFIED,
    UAT_NOTIFIED;
}
