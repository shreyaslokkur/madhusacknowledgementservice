package com.lks.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 25/4/17
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class Context {

    private List<MASException> exceptionList = new ArrayList<>();
    private List<MASWarning> warningList = new ArrayList<>();

    public List<MASException> getExceptionList() {
        return exceptionList;
    }

    public void setExceptionList(List<MASException> exceptionList) {
        this.exceptionList = exceptionList;
    }

    public List<MASWarning> getWarningList() {
        return warningList;
    }

    public void setWarningList(List<MASWarning> warningList) {
        this.warningList = warningList;
    }
}
