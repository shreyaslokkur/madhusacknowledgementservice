package com.lks.core;

import com.lks.db.dao.UserDAO;
import com.lks.db.qo.UserQO;
import com.lks.models.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lokkur on 02/09/17.
 */
public class FilterationService {

    @Autowired
    UserDAO userDAO;

    public List<TransactionDTO> filter(List<TransactionDTO> transactionDTOList) {
        /**
         * Seperate the entries with pre-existing vehicle number
         * Those which have a pre-existing vehicle number, check if entries exist with same phone and email address
         * Those which have a pre-existing vehicle number, check if entries exist with same phone/email and different email/phone. Update those entries and mark them for repeat Notification
         *
         */
        List<String> vehicleNumberInXML = new ArrayList<>();
        for(TransactionDTO transactionDTO : transactionDTOList) {
            if(transactionDTO.getVehicleNumber() != null) {
                vehicleNumberInXML.add(transactionDTO.getVehicleNumber());
            }
        }
        if(vehicleNumberInXML.size() > 0) {
            List<UserQO> preExistingUsers = userDAO.getValidVehicleNumberPresentInList(vehicleNumberInXML);
            if (preExistingUsers != null) {
                for (UserQO user : preExistingUsers) {
                    Iterator iter = transactionDTOList.iterator();
                    TransactionDTO transactionDTO;
                    while (iter.hasNext()) {
                        transactionDTO = (TransactionDTO) iter.next();
                        if (user.getVehicleNumber().equals(transactionDTO.getVehicleNumber())) {
                            //Check if the phone and the email address is the same
                            boolean needsUpdate = false;
                            if ((user.getEmailAddress() != null && transactionDTO.getEmailAddress()!= null) && (!user.getEmailAddress().equals(transactionDTO.getEmailAddress()))) {
                                //update Email Address
                                user.setEmailAddress(transactionDTO.getEmailAddress());
                                needsUpdate = true;
                            } else if ((user.getPhoneNumber() != null && transactionDTO.getPhoneNumber() != null) && (!user.getPhoneNumber().equals(transactionDTO.getPhoneNumber()))) {
                                //update Phone number
                                needsUpdate = true;
                                user.setPhoneNumber(transactionDTO.getPhoneNumber());
                            }
                            if (needsUpdate) {
                                userDAO.updateUser(user);
                            }
                            iter.remove();
                        }
                    }
                }

            }
        }
        return transactionDTOList;
    }
}
