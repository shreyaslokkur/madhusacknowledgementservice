package com.lks.core;

/**
 * Created by lokkur on 31/03/17.
 */
public enum ServiceType {
    PURCHASE_TYRE,
    WHEEL_BALANCING,
    WHEEL_ALIGNMENT,
    NOT_KNOWN,
    TOTAL_WHEEL_SERVICE;
}
