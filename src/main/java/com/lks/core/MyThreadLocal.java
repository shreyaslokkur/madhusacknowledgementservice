package com.lks.core;

/**
 * Created with IntelliJ IDEA.
 * User: shreyaslokkur
 * Date: 25/4/17
 * Time: 5:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyThreadLocal {

    public static final ThreadLocal userThreadLocal = new ThreadLocal(){
        @Override
        protected Object initialValue() {
            return new Context();
        }
    };

    public static void set(Context context) {
        userThreadLocal.set(context);
    }

    public static void unset() {
        userThreadLocal.remove();
    }

    public static Context get() {
        return (Context) userThreadLocal.get();
    }
}
