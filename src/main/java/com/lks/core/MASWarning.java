package com.lks.core;

public class MASWarning extends RuntimeException {

    private String message;
    private MASWarningCodes warningCodes;

    public MASWarning(MASWarningCodes warningCodes, String message) {
        super(message);
        this.message = message;
        this.warningCodes = warningCodes;
    }

    public MASWarning(MASWarningCodes masErrorCodes, String message, Throwable cause) {

        super(message, cause);
        this.message = message;
        this.warningCodes = masErrorCodes;
    }

    public MASWarning(Throwable cause) {
        super(cause);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MASWarningCodes getWarningCodes() {
        return warningCodes;
    }

    public void setWarningCodes(MASWarningCodes warningCodes) {
        this.warningCodes = warningCodes;
    }
}
