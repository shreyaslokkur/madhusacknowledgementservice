package com.lks.models;

/**
 * Created by lokkur on 15/02/18.
 */
public class SMSBalance {

    private Balance balance;
    private String status;

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class Balance {
        String sms;

        public String getSms() {
            return sms;
        }

        public void setSms(String sms) {
            this.sms = sms;
        }
    }



}
