package com.lks.models;

/**
 * Created by lokkur on 01/04/17.
 */
public class AcknowledgementDTO {

    private int notificationId;
    private boolean isEmailNotificationSent;
    private boolean isSMSNotificationSent;

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public boolean isEmailNotificationSent() {
        return isEmailNotificationSent;
    }

    public void setEmailNotificationSent(boolean isEmailNotificationSent) {
        this.isEmailNotificationSent = isEmailNotificationSent;
    }

    public boolean isSMSNotificationSent() {
        return isSMSNotificationSent;
    }

    public void setSMSNotificationSent(boolean isSMSNotificationSent) {
        this.isSMSNotificationSent = isSMSNotificationSent;
    }

    @Override
    public String toString() {
        return "AcknowledgementDTO{" +
                "notificationId='" + notificationId + '\'' +
                ", isEmailNotificationSent=" + isEmailNotificationSent +
                ", isSMSNotificationSent=" + isSMSNotificationSent +
                '}';
    }
}
