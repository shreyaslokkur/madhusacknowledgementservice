
package com.lks.models;

import com.lks.core.NotificationStatus;
import com.lks.core.ServiceType;

public class TransactionDTO {

    private int id;
    private String name;
    private String phoneNumber;
    private String emailAddress;
    private String vehicleNumber;
    private double odometerReading;
    private ServiceType serviceType;
    private NotificationStatus notificationStatus;
    private boolean isRepeat;
    private String vehicleBrandName;
    private String vehicleModelName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public double getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(double odometerReading) {
        this.odometerReading = odometerReading;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public void setRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }

    public String getVehicleBrandName() {
        return vehicleBrandName;
    }

    public void setVehicleBrandName(String vehicleBrandName) {
        this.vehicleBrandName = vehicleBrandName;
    }

    public String getVehicleModelName() {
        return vehicleModelName;
    }

    public void setVehicleModelName(String vehicleModelName) {
        this.vehicleModelName = vehicleModelName;
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", odometerReading=" + odometerReading +
                ", serviceType=" + serviceType +
                ", notificationStatus=" + notificationStatus +
                ", isRepeat=" + isRepeat +
                ", vehicleBrandName='" + vehicleBrandName + '\'' +
                ", vehicleModelName='" + vehicleModelName + '\'' +
                '}';
    }
}
