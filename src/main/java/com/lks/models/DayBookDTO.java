package com.lks.models;

import java.util.List;

/**
 * Created by lokkur on 27/08/17.
 */
public class DayBookDTO {

    private String name;
    private List<String> addressList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<String> addressList) {
        this.addressList = addressList;
    }

    @Override
    public String toString() {
        return "DayBookDTO{" +
                "name='" + name + '\'' +
                ", addressList=" + addressList +
                '}';
    }
}
