package com.lks;

import com.lks.connector.TallyConnector;
import com.lks.db.dao.NotificationDAO;
import com.lks.generator.AcknowledgementServiceGenerator;
import com.lks.models.TransactionDTO;
import com.lks.notifications.EmailNotification;
import com.lks.notifications.SMSNotification;
import com.lks.service.NotificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MadhusAcknowledgementServiceApplication.class)
@WebAppConfiguration
public class PersonalPortfolioTrackerApplicationTests {


    @Autowired
    NotificationDAO notificationDAO;

    @Autowired
    AcknowledgementServiceGenerator acknowledgementServiceGenerator;

    @Autowired
    TallyConnector tallyConnector;

    @Autowired
    NotificationService notificationService;

    @Autowired
    SMSNotification smsNotification;

    @Autowired
    EmailNotification emailNotification;




    @Test
    public void testCompleteWorkflow() {
        notificationService.execute();

    }

    /*@Test
    public void testDatabaseInsert() {
        List<User> userList = csvParser.parseCSV();
        NotificationQO notificationQO = new NotificationQO();
        for(User user : userList) {
            notificationQO.setName(user.getName());
            notificationQO.setPhoneNumber(user.getPhoneNumber());
            notificationQO.setEmailAddress(user.getEmail());
            notificationQO.setVehicleNumber(user.getVehicleNumber());
            notificationQO.setServiceType(ServiceType.TOTAL_WHEEL_SERVICE.name());
        }
        int key = notificationDAO.addNotification(notificationQO);
        Assert.assertNotEquals(key, 0);

    }*/

    /*@Test
    public void testDayBookParser() {
        DayBookParser dayBookParser = new DayBookParser();
        List<TransactionDTO> transactionDTOList = dayBookParser.parseDayBook("static/DayBook.xml");
        Assert.assertNotNull(transactionDTOList);

    }*/

    /*@Test
    public void testTallyConnector() {
        tallyConnector.retrieveFromTally();

    }*/

    @Test
    public void testSMSNotification() {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setVehicleNumber("KA05HC5890");
       /* transactionDTO.setEmailAddress("nikhilb2008@gmail.com");*/
        transactionDTO.setPhoneNumber("9900496841");
        transactionDTO.setName("Prasanna");
        transactionDTO.setRepeat(false);
        smsNotification.sendSms(transactionDTO);

    }

    @Test
    public void testEmailNotification() {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setVehicleNumber("KA05HC5890");
        transactionDTO.setEmailAddress("nikhilb2008@gmail.com");
        transactionDTO.setPhoneNumber("9008400738");
        transactionDTO.setName("Nikhil");
        transactionDTO.setRepeat(false);
        emailNotification.generateAndSendEmail(transactionDTO);

    }



}
